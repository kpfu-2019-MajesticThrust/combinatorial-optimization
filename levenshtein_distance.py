def lev(a: str, b: str) -> int:
    """
    Levenshtein distance..
    """
    m = len(a)
    n = len(b)
    # m+1 * n+1 values matrix
    d = [[0 for j in range(n + 1)] for i in range(m + 1)]

    # source prefixes can be transformed into empty string by removing all characters
    for i in range(1, m + 1):
        d[i][0] = i

    # target prefixes can be reached from empty source string by inserting every character
    for j in range(1, n + 1):
        d[0][j] = j

    for j in range(1, n + 1):
        for i in range(1, m + 1):
            substitutionCost = 0 if a[i - 1] == b[j - 1] else 1

            d[i][j] = min(
                d[i - 1][j] + 1,  # deletion
                d[i][j - 1] + 1,  # insertion
                d[i - 1][j - 1] + substitutionCost)  # substitution

    return d[m][n]


if __name__ == "__main__":
    pairs = [
        ("a", "b"),
        ("more", "less"),
        ("sitting", "kitten"),
        ("sunday", "saturday")
    ]

    for a, b in pairs:
        print("{} -> {}: {}".format(a, b, lev(a, b)))
