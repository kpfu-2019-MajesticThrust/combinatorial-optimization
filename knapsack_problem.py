from typing import NamedTuple, AbstractSet, Dict


class Item(NamedTuple):
    name: str
    weight: int
    value: int


def UKP(items: AbstractSet[Item], max_weight: int) -> Dict[Item, int]:
    """Unbounded Knapsack Problem"""

    # best value for each weight
    # weight 0 has value 0
    weight_values = [0] + [None] * max_weight

    # items for each weight
    # no item for 0 weight
    weight_items = [None] + [None] * max_weight

    # find best items for each weight
    for w in range(1, max_weight + 1):
        # filter all items that can fit under selected weight
        available_items = filter(
            lambda item: item != None and item.weight <= w, items)

        # calculate value impact for every available item
        choices = map(
            lambda item: (item, item.value + weight_values[w - item.weight]),
            available_items)

        # select max value item
        choice = max(choices, key=lambda c: c[1])

        max_item, value = choice

        weight_items[w] = max_item
        weight_values[w] = value

    result = {}
    # get best item set
    i = max_weight
    while i > 0:
        item = weight_items[i]
        result[item] = result.get(item, 0) + 1
        i -= item.weight

    return result


if __name__ == "__main__":
    items = [
        Item(name="Fork", weight=1, value=2),
        Item(name="Spork", weight=2, value=4),
        Item(name="Frying pan", weight=5, value=11),
        Item(name="Canned food", weight=4, value=6),
        Item(name="Axe", weight=4, value=7),
        Item(name="Knife", weight=3, value=5)
    ]

    max_weight = 22

    result = UKP(set(items), max_weight)
    total_value = sum(map(lambda i: i.value * result[i], result))

    print("Best-value contents of the knapsack:")

    for item in result:
        print("\t- {} of '{}'".format(result[item], item.name))

    print("Total value: {}".format(total_value))