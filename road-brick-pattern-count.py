def create_counter_3():
    # 0 length has 1 pattern of distribution
    cache = [1]

    def calculate_from_cache(n: int):
        # odd road lengths have 0 ways to place 1x2 blocks without gaps
        if n % 2 == 1:
            return 0

        # static 3x2 block (3 variants)
        k = 3 * cache[n - 2]

        # "stretching" 3xM blocks, M mod 2 = 0 (2 variants each)
        for m in range(4, n + 2, 2):
            k += 2 * cache[n - m]

        return k

    def count_patterns_3(n: int):
        """Amount of patterns for a 1x2 block and a road of width 3

        Arguments
        ---------
            n (int): road length
        """

        # calculate all preceding
        for i in range(len(cache), n + 1):
            k = calculate_from_cache(n)
            cache.append(k)

        return cache[n]

    return count_patterns_3


if __name__ == "__main__":
    counter = create_counter_3()

    for i in range(20):
        print("Road length: {}, # patterns: {}".format(i, counter(i)))
